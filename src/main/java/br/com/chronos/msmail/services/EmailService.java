package br.com.chronos.msmail.services;

import br.com.chronos.msmail.enums.StatusEmail;
import br.com.chronos.msmail.models.Email;
import br.com.chronos.msmail.models.dtos.EmailDTO;
import br.com.chronos.msmail.repositories.EmailRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EmailService {
    private final EmailRepository emailRepository;
    private final JavaMailSender mailSender;

    public EmailService(final EmailRepository emailRepository, JavaMailSender mailSender) {
        this.emailRepository = emailRepository;
        this.mailSender = mailSender;
    }
    public Email sendingEmail(EmailDTO emailDto) {
        Email email = new Email();
        BeanUtils.copyProperties(emailDto, email);

        email.setSendDateEmail(LocalDateTime.now());
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(email.getEmailFrom());
            message.setTo(email.getEmailTo());
            message.setSubject(email.getSubject());
            message.setText(email.getContent());
            mailSender.send(message);

            email.setStatusEmail(StatusEmail.SENT);
        } catch(MailException e) {
            email.setStatusEmail(StatusEmail.ERROR);
        } finally {
             return emailRepository.save(email);
        }
    }
}
