package br.com.chronos.msmail.controllers;

import br.com.chronos.msmail.models.Email;
import br.com.chronos.msmail.models.dtos.EmailDTO;
import br.com.chronos.msmail.services.EmailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/mail")
public class EmailController {

    private final EmailService emailService;

    public EmailController(final EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping
    public ResponseEntity<Email> sendingEmail(@RequestBody @Valid EmailDTO emailDTO) {
        var email = emailService.sendingEmail(emailDTO);
        return new ResponseEntity<>(email, HttpStatus.CREATED);
    }
}
