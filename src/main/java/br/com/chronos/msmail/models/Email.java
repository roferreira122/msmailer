package br.com.chronos.msmail.models;

import br.com.chronos.msmail.enums.StatusEmail;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name="TB_EMAIL")
public class Email implements Serializable {
    private static final long serialVersionUID = 1;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private String ownerRef;

    private String emailFrom;

    private String emailTo;

    private String subject;

    @Column(columnDefinition = "TEXT")
    private String content;
    private boolean html;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime sendDateEmail;

    private StatusEmail statusEmail;
}
