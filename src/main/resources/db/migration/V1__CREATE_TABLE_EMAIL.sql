create table tb_email (
    id uuid not null,
    content TEXT not null,
    email_from varchar(255) not null,
    email_to varchar(255) not null,
    html boolean default false,
    owner_ref varchar(255),
    send_date_email TIMESTAMP,
    status_email int4 not null,
    subject varchar(255),
    primary key (id)
);